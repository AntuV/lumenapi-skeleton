## Lumen 5.1 + Dingo API + JWT Auth

Working example of Dingo API using JWT Auth to secure endpoints.
CORS complaint.

Easy to clone for a new project or just to get over the obstacles of your own implementation.

Original implementation by [0plus1](https://github.com/0plus1/lumendingojwtapi)
